USE mp;

create index fecha on transaccion(fecha(100));
create index estatus on transaccion(estatus(1));
SELECT 
    mes, A / (A + R) AS TA
FROM
    (SELECT 
        mes,
            SUM(CASE
                WHEN estatus = 'A' THEN ntxn
                ELSE 0
            END) AS A,
            SUM(CASE
                WHEN estatus = 'R' THEN ntxn
                ELSE 0
            END) AS R
    FROM
        (SELECT 
        YEAR(fecha) * 100 + MONTH(fecha) AS mes,
            estatus,
            COUNT(*) AS ntxn
    FROM
        transaccion
    GROUP BY YEAR(fecha) * 100 + MONTH(fecha) , estatus) A
    GROUP BY mes) B
;
 